<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\SortableTrait;

class Service extends Model
{
    use HasFactory, SortableTrait, Translatable;

    public $translatedAttributes = ['title', 'text'];

    public $sortable = [
        'order_column_name'     => 'sort_order',
        'sort_when_creating'    => true,
    ];
}
