<?php

namespace App\Http\Controllers;

use App\Mail\ContactRequestMail;
use App\Models\Commodity;
use App\Models\ContactRequest;
use App\Models\Employer;
use App\Models\Menu;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        list($menus, $commodities, $services, $employers) = [
            Menu::ordered()->get(),
            Commodity::ordered()->get(),
            Service::ordered()->get(),
            Employer::ordered()->get()
        ];

        return view('home', [
            'menus'         =>  $menus,
            'commodities'   =>  $commodities,
            'services'      =>  $services,
            'employers'     =>  $employers,
        ]);
    }

    public function termsAndConditions()
    {
        return view('terms-and-conditions',  [
            'menus' =>  Menu::ordered()->get(),
        ]);
    }

    public function contact(Request $request)
    {
        $contactRequest =  ContactRequest::create($request->all());

        if($contactRequest) {

            foreach (['info@cct.az', 'fr@cct.az',] as $email) {
                Mail::to($email)
                    ->send(new ContactRequestMail(
                        $contactRequest
                    ));
            }
        }

        return [
            'message'   =>  (bool) $contactRequest ? __('front.result-success') : __('front.result-error')
        ];
    }
}
