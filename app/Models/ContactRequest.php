<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactRequest extends Model
{
    use HasFactory;

    protected $casts = [
        'contacted_at'  =>  'datetime'
    ];

    protected $fillable = ['name', 'email', 'title', 'text', 'is_contacted', 'contacted_at'];
}
