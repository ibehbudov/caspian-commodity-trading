<?php

namespace Database\Seeders;

use App\Models\ContactRequest;
use App\Models\Menu;
use App\Models\User;
use Illuminate\Database\Seeder;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        ContactRequest::factory(100)->create();

        User::create([
            'name'      =>  'Administrator',
            'email'     =>  'admin@example.com',
            'password'  =>  bcrypt(123456),
        ]);

        foreach (['commodities', 'services', 'team', 'contact'] as $menuName) {

            $menu = [];

            foreach (LaravelLocalization::getSupportedLocales() as $localeKey => $locale) {

                $menu['name:' . $localeKey] = ucfirst($menuName);
            }

            $menu['slug'] = $menuName;

            Menu::create($menu);
        }
    }
}
