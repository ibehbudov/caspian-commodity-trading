<section id="team">
    <div class="padding-x">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-5 d-flex align-items-center">
                <div class="w-100">
                    <div class="left-heading d-flex align-items-center">
                        <img src="{{ asset('img/icons/Next1.png') }}" alt="next-icon" class="arrow-icon"> <span
                            class="section-heading">{{ __('front.team') }}</span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 col-lg-7">
                <p class="about-team">{{ __('front.team-description') }}</p>
            </div>

            <div class="col-12 mt-5">
                <div class="row">
                    <div class="owl-carousel owl-theme" id="teamSlider">
                        @foreach($employers as $employer)
                        <div class="item">
                            <div class="team-parts">
                                <div class="w-100 team-part">
                                    <img src="{{ asset('storage/' . $employer->image) }}" alt="team1" class="w-100">
                                    <div class="team-name-pos">
                                        <span>{{ $employer->name }}</span>
                                        <span>{{ $employer->job_title }}</span>
                                    </div>
                                    <div class="team-part-overlay">
                                        <div class="overlay-text">
                                            <p>
                                                {{ strip_tags($employer->text) }}
                                                <br><br>
                                                <b>{{ $employer->email }}</b>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
