<section id="services">
    <div class="padding-x">

        <div class="w-100">
            <div class="left-heading d-flex align-items-center">
                <img src="{{ asset('img/icons/Next1.png') }}" alt="next-icon" class="arrow-icon"> <span
                    class="section-heading">{{ __('front.services') }}</span>
            </div>
        </div>

        <div class="service-parts w-100">
            <div class="row">

                @php($serviceCounter = 0)

                @foreach($services as $service)

                    <div class="col-lg-4 col-md-6 col-12 @if($serviceCounter) mt-3 @endif ">
                        <div class="w-100 @if($serviceCounter) d-flex justify-content-end @endif ">
                            <div class="service-heading">
                                <span>{{ $service->title }}</span>
                            </div>
                            <div class="service-body">
                                <p>{{ strip_tags($service->text) }}</p>
                            </div>
                        </div>
                    </div>

                    {{--@if($loop->iteration % 3 == 0)
                        @php($serviceCounter = !$serviceCounter)
                    @endif--}}

                @endforeach

            </div>
        </div>

    </div>
</section>
