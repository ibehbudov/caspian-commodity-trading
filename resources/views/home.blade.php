@extends('layout')

@section('content')

    <!-- HEADER -->
    <section id="header">
        <div class="header-overlay"></div>
        <!-- navbar -->
        <div class="navigation-wrap start-header start-style">
            <div class="padding-x">
                <div class="row">
                    <div class="col-12">
                        <nav class="navbar navbar-expand-md navbar-light">

                            <a class="navbar-brand" href="#">
                                <img src="{{ asset('img/logo.png') }}" alt="Caspian Commodity Trading">
                            </a>

                            <button class="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto py-4 py-md-0">

                                    <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                                        <a class="nav-link " href="{{ route('home') }}">{{ __('front.home') }}</a>
                                    </li>

                                    @foreach($menus as $menu)
                                        <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                                            <a class="nav-link" href="#{{ $menu->slug }}">{{ $menu->name }}</a>
                                        </li>
                                    @endforeach

                                    <li class="nav-item dropdown active-style pl-md-0 ml-0 ml-md-1 ml-lg-4">
                                        <a class="nav-link dropdown-toggle " href="#" id="dropdown09"
                                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="flag-icon flag-icon-{{ str_replace('en', 'gb', LaravelLocalization::getCurrentLocale()) }}"> </span>
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdown09">

                                            @foreach(LaravelLocalization::getSupportedLocales() as $localeKey => $locale)
                                                <a class="dropdown-item" href="{{ url($localeKey) }}">
                                                    <span class="flag-icon flag-icon-{{ str_replace('en', 'gb', $localeKey) }}"> </span>
                                                </a>
                                            @endforeach
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="main w-100">
            <div class="padding-x">
                <div class="row">
                    <div class="col-12 col-md-8 col-lg-6">
                        <span class="main-heading">{{ __('front.heading_1') }}</span>
                        <span class="main-heading">{{ __('front.heading_2') }}</span>
                    </div>

                </div>
                <div class="row">
                    <div class="col-12 col-md-7 col-lg-5 main-text">
                        <span class="">{{ __('front.subheading') }}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="w-100">
                            <div class="main-foot d-flex align-items-center">
                                <img src="{{ asset('img/icons/Next.png') }}" alt="next-icon" class="arrow-icon">
                                <a href="#contact" class="main-foot-text">{{ __('front.contact') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- scroll down -->
        <div class="field">
            <div class="scroll-down">
                <div class="arrow"></div>
            </div>
        </div>
    </section>

    <div id="sections-ordered">
        @foreach($menus as $menu)
            @includeIf('sections.' . $menu->slug)
        @endforeach
    </div>

@endsection
