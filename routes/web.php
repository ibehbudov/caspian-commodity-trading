<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => LaravelLocalization::setLocale()], function() {

    Route::get('/', [HomeController::class, 'index'])
        ->name('home');

    Route::post('contact', [HomeController::class, 'contact'])
        ->name('contact');

    Route::get('terms-and-conditions.html', [HomeController::class, 'termsAndConditions'])
        ->name('terms');
});


Route::get('admin', function (){
    return redirect('nova');
});
