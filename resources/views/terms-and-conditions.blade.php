@extends('layout')

@section('content')
    <section id="header" class="terms-header">
        <div class="header-overlay"></div>
        <!-- navbar -->
        <div class="navigation-wrap start-header start-style">
            <div class="padding-x">
                <div class="row">
                    <div class="col-12">
                        <nav class="navbar navbar-expand-md navbar-light">

                            <a class="navbar-brand" href="{{ route('home') }}">
                                <img src="{{ asset('img/logo.png') }}" alt="{{ __('front.title') }}">
                            </a>

                            <button class="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto py-4 py-md-0">

                                    <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                                        <a class="nav-link" href="{{ route('home') }}">{{ __('front.home') }}</a>
                                    </li>

                                    @foreach($menus as $menu)
                                        <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                                            <a class="nav-link" href="{{ url('/#' . $menu->slug) }}">{{ __('front.' . $menu->slug) }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="terms-main w-100">
            <div class="padding-x">
                {!! __('front.term-and-cond-header-text') !!}
            </div>
        </div>
    </section>

    <section id="terms-body">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="w-100 d-flex justify-content-end terms-use">
                        {!! __('front.term-and-cond-subheader-text') !!}
                    </div>
                </div>

                <div class="col-12">
                    <div class="w-100 terms-head">
                        <span class="terms-heading">{{ __('front.terms-title-1') }}</span>
                    </div>

                    <p class="terms-text">{!! nl2br(__('front.terms-text-1')) !!}</p>

                    <div class="w-100 terms-head">
                        <span class="terms-heading">{{ __('front.terms-title-2') }}</span>
                    </div>
                    <p class="terms-text">
                        <span class="font-weight-bold">{!! nl2br(__('front.terms-text-2')) !!}</span>
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
