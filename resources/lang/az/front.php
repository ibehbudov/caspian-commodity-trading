<?php

return array (
  'address-1-address' => 'SAT Plaza, 133 Bashir Safaroglu 1007 Baku, Azerbaijan',
  'address-1-email' => 'info@cct.az
fr@cct.az',
  'address-1-phones' => '012 409 36 09; 050 204 99 66',
  'address-2-address' => '',
  'address-2-email' => '',
  'address-2-phones' => '',
  'commodities' => 'Commodities',
  'contact' => 'Contact',
  'contact-header-text' => 'Looking forward for cooperation with companies that share our values and thinking BIG.',
  'contact-mail' => 'Email',
  'contact-message' => 'Message',
  'contact-name-surname' => 'Name, Surname',
  'contact-send-message' => 'Send',
  'contact-subject' => 'Subject',
  'heading_1' => 'We understand',
  'heading_2' => 'how markets work',
  'home' => 'Home',
  'result-error' => 'Error while sending',
  'result-success' => 'Message sent',
  'services' => 'Services',
  'site_name' => 'Caspian Commodity Trading.',
  'subheading' => 'Providing our clients with market information that is timely,accurate, reliable, relevant and unbiased.',
  'team' => 'Our Team',
  'team-description' => 'ssLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
  'term-and-cond-header-text' => '<span>Terms and</span><span> Conditions</span>',
  'term-and-cond-subheader-text' => '<span>Caspian Commodity Trading
           <span class="font-weight-bold">Website Terms of Use</span>
</span>',
  'terms-and-conditions' => 'Terms and Conditions',
  'terms-text-1' => 'Please read these caspianct.com Website Terms of Use (“Terms of Use”) carefully before using the Caspian Commodity Trading website (the “Website”).

By browsing, accessing or otherwise using the Website, you agree to accept these Terms of Use.  Your visit to the Website, including, but not limited to, all images, text, illustrations, designs, icons, photographs, programs, music clips, downloads, systems and methods of trading, video clips, graphics, user interfaces, visual interfaces, information, data, tools, products, written materials, services, design, structure, selection, coordination, expression content, information, design, marks and layout therein, including but not limited to the design, structure, selection, coordination, expression and arrangement of any of the foregoing available on or through the Website, together (the “Content”), is subject to these Terms of Use, our Privacy Policy and our Cookies Policy, which may be updated by us at any time without notice to you. 

If we update our Terms of Use, we will post the updated Terms of Use on the Website. Any such changes will be effective upon posting. If you do not agree with these Terms of Use, you should not use the Website and you agree that your sole and exclusive remedy is to discontinue using the Website. The Website is not designed for use by minors and minors should not provide personal information through this Website.  Nothing contained in these Terms of Use is intended to modify any other written agreement you may have with Brookfield (if any) that may be in effect. Additional terms and conditions of use may be applicable to password-restricted areas of the Website.  For the purposes of these Terms of Use, “Caspian Commodity Trading,” “we” and “us” means the Caspian Commodity Trading entity which controls this Website and the “User” means any person accessing the Website. The websites of other Brookfield affiliates are governed by separate terms of use.  Any disputes between you and Brookfield in connection with these Terms of Use may be resolved in arbitration or small claims court, subject to applicable law.',
  'terms-text-2' => 'You are responsible for your use of the Website and the Content. 
You undertake to use your best endeavors to ensure that nothing you do whilst accessing the 
Website will damage the Website or otherwise violate these Terms of Use.',
  'terms-title-1' => 'INTRODUCTION',
  'terms-title-2' => 'USE OF THE WEBSITE AND CONTENT',
  'title' => 'Caspian Commodity Trading',
);
