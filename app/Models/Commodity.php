<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\SortableTrait;

class Commodity extends Model
{
    use HasFactory, SortableTrait, Translatable;

    public $translatedAttributes = ['name', 'text'];

    public $sortable = [
        'order_column_name'     => 'sort_order',
        'sort_when_creating'    => true,
    ];

    public function getOptionsAttribute()
    {
        $text = strip_tags(str_replace(['<br>', '<li>'], PHP_EOL, $this->text));

        return array_filter(explode(PHP_EOL, $text));
    }

    public function getFirstOptionsAttribute()
    {
        return array_slice($this->options, 0, count($this->options) / 2);
    }

    public function getSecondOptionsAttribute()
    {
        return array_slice($this->options, count($this->options) / 2);
    }
}
