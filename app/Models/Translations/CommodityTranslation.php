<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommodityTranslation extends Model
{
    use HasFactory;

    public $fillable = ['name', 'text'];
}
