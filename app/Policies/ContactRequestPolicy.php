<?php

namespace App\Policies;

use App\Models\ContactRequest;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContactRequestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ContactRequest  $contactRequest
     * @return mixed
     */
    public function view(User $user, ContactRequest $contactRequest)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ContactRequest  $contactRequest
     * @return mixed
     */
    public function update(User $user, ContactRequest $contactRequest)
    {
        if(request()->post('resources')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ContactRequest  $contactRequest
     * @return mixed
     */
    public function delete(User $user, ContactRequest $contactRequest)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ContactRequest  $contactRequest
     * @return mixed
     */
    public function restore(User $user, ContactRequest $contactRequest)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ContactRequest  $contactRequest
     * @return mixed
     */
    public function forceDelete(User $user, ContactRequest $contactRequest)
    {
        //
    }
}
