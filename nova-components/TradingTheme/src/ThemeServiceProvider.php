<?php

namespace Ibehbudov\TradingTheme;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Nova;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Nova::booted(function () {
            //Nova::theme(asset('/ibehbudov/trading-theme/theme.css'));
        });

        $this->publishes([
            __DIR__.'/../resources/css' => public_path('ibehbudov/trading-theme'),
        ], 'public');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
