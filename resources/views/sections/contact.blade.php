<!-- Contact -->
<section id="contact">
    <div class="padding-x">

        <div class="w-100 d-flex justify-content-end mb-3">
            <div class="right-heading d-flex align-items-center">
                <span class="section-heading">{{ __('front.contact') }}</span>
                <img src="{{ asset('img/icons/Prev.png') }}" alt="next-icon" class="arrow-icon">
            </div>
        </div>

        <div class="row">

            <!-- Contact form -->
            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-12 contact-heading">
                            <span>{{ __('front.contact-header-text') }}</span>
                    </div>
                    <div class="col-12 contact-form mt-5">
                        <div class="input-padding-x">
                            <input required type="text" name="namesurname" class="namesurname" id="namesurname"/>
                            <label>{{ __('front.contact-name-surname') }}</label>
                        </div>

                        <div class="input-padding-x">
                            <input required type="text" name="email" class="email" id="email" />
                            <label>{{ __('front.contact-mail') }}</label>
                        </div>

                        <div class="input-padding-x">
                            <input required type="text" name="subject" class="subject" id="subject" />
                            <label>{{ __('front.contact-subject') }}</label>
                        </div>

                        <div class="input-padding-x message-put">
                            <textarea required name="message" class="message" id="message"></textarea>
                            <label>{{ __('front.contact-message') }}</label>
                        </div>

                        <button class="contact-send" id="contact-send">{{ __('front.contact-send-message') }}</button>
                    </div>
                    <div class="col-6 contact-address mt-5">
                            <span>
                                <span class="font-weight-bold">{!! nl2br(__('front.address-1-address')) !!}</span>
                                <br>
                                {!! nl2br(__('front.address-1-phones')) !!}<br>
                                {!! nl2br(__('front.address-1-email')) !!}
                            </span>
                    </div>
                    <div class="col-6 contact-address mt-5">
                        <span>
                                <span class="font-weight-bold">{!! nl2br(__('front.address-2-address')) !!}</span>
                                <br>
                                {!! nl2br(__('front.address-2-phones')) !!} <br>
                                {!! nl2br(__('front.address-2-email')) !!}
                            </span>
                    </div>
                </div>
            </div>

            <!-- Map -->
            <div class="col-12 col-md-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3039.5843025897007!2d49.831144915586435!3d40.37374086617649!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40307dba35bd036f%3A0x35060ff873c1bc1b!2sSat%20Plaza!5e0!3m2!1sen!2sus!4v1619225085401!5m2!1sen!2sus" width="600" height="450" style="width: 100%; height: 100%;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>

    </div>
</section>
