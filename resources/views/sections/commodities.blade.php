<section id="commodities">
@foreach($commodities as $commodity)
    <!-- Commodities part -->
    <div class="commodities-part">
        <div class="padding-x">
            <div class="w-100 c-heading ">
                <div class="d-flex align-items-center commodities-part-heading">
                    <span class="section-heading ">{{ __('front.commodities') }}</span>
                </div>
            </div>
            <div class="w-100 c-heading ">
                <div class="d-flex align-items-center commodities-subtitles">
                    <span>{{ $commodity->name }}</span>
                </div>
            </div>

            <div class="col-12 c-all">
                <div class="row mt-3 ">
                    <div class="col-12 col-md-12 col-lg-3 c-one"></div>
                    <div class="col-12 col-md-4 col-lg-3 c-two">
                        <ul class="commodities-list">
                            @foreach($commodity->firstOptions as $option)
                            <li>{!! $option !!}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-12 col-md-4 col-lg-3 c-three">
                        <ul class="commodities-list">
                            @foreach($commodity->secondOptions as $option)
                                <li>{!! $option !!}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-12 col-md-4 col-lg-3 c-four">
                        <div class="w-100 commodities-img">
                            <img src="{{ asset('storage/' . $commodity->image) }}" alt="{{ $commodity->name }}" class="w-100">
                            <div class="overlay"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endforeach
</section>
