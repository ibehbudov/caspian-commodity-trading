<!-- Footer -->
<section id="footer">
    <div class="padding-x">
        <div class="row">
            <div class="col-12 col-md-4 d-flex align-items-center footer-logo">
                <a class="navbar-brand" href="{{ route('home') }}">
                    <img src="{{ asset('img/logo.png') }}" alt="Caspian Commodity Trading">
                </a>
            </div>
            <div class="col-12 col-md-8">
                <div class="row">
                    <div class="col-12">
                        <div class="w-100 d-flex justify-content-end s-medias">
                            <ul class="list-unstyled d-inline-flex social-medias">
                                {{--<li class="list-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="list-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li class="list-item"><a href=""><i class="fab fa-instagram"></i></a></li>
                                <li class="list-item"><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                <li class="list-item"><a href="#"><i class="fab fa-youtube"></i></a></li>--}}
                            </ul>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="w-100 d-md-flex justify-content-end">
                            <a href="{{ route('terms') }}" class="terms">{{ __('front.terms-and-conditions') }}</a>
                            <span class="copyright">&copy; {{ now()->format('Y') }}. All Right Reserved. Developed by <a href="https://webcoder.az" class="font-weight-bold" style="text-decoration: none; color: black">Webcoder</a></span>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
        integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw=="
        crossorigin="anonymous"></script>

<script src="js/script.js"></script>
