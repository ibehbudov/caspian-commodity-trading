document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});
$(document).ready(function() {
    $(".scroll-down").click(function() {
         $('html, body').animate({
             scrollTop: $("#sections-ordered").offset().top
         }, 1000);
    });
});
$('#teamSlider').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
	dots:false,
	autoplay:true,
	autoplayTimeout: 5000,
    responsive:{
        0:{
            items:1
        },
        520:{
            items:2
        },
        768:{
            items:2
        },
        1024:{
            items:2
        },
        1220:{
            items:3
        }
    }
});

$('#commoditiesSlider').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
	dots:false,
	autoplay:true,
	autoplayTimeout: 5000,
    responsive:{
        0:{
            items:1
        },
        520:{
            items:1
        },
        768:{
            items:1
        }
    }
});

$('#contact-send').click(function() {

    $('.errorInput').removeClass("errorInput")

    let namesurname = $('#namesurname').val();
    let subject = $('#subject').val();
    let email = $('#email').val();
    let message = $('#message').val();

    if (namesurname.length==0) {
        $("#namesurname").addClass("errorInput")
    }
    if(subject.length==0){
        $("#subject").addClass("errorInput")
    }
    if(email.length==0){
        $("#email").addClass("errorInput")
    }
    if(message.length==0){
        $("#message").addClass("errorInput")
    }
    if(IsEmail(email)==false){
        $("#email").addClass("errorInput")
    }

    if($('.errorInput').length === 0) {

        $.post('/contact', {
            name: namesurname,
            title: subject,
            email: email,
            text: message
        })
            .done(function (response){
                $("#contact-send").html('<i class="fas fa-check"></i>' + response.message);
                $("#contact-send").addClass("sended");

                $('#contact-send').prop('disabled', true)

            });
    }

})
function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
       return false;
    }else{
       return true;
    }
  }
